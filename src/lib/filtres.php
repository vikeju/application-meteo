<?php
$filtres=[
    [
        'libelle'=>'Moins de 10°',
        'val_min'=>-1000,
        'val_max'=>9.9
    ],

    [
        'libelle'=>'Entre 10° et 15°',
        'val_min'=>10,
        'val_max'=>14.9
    ],

    [
        'libelle'=>'Entre 15° et 20°',
        'val_min'=>15,
        'val_max'=>19.9
    ],

    [
        'libelle'=>'Entre 20° et 25°',
        'val_min'=>20,
        'val_max'=>24.9
    ],

    [
        'libelle'=>'Plus de 25°',
        'val_min'=>25,
        'val_max'=>1000
    ]
];