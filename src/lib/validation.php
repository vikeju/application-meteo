<?php
// require "../kint.phar";

// foreach ($_POST as $key => $value) :
//     $key;
//     $value;
//     ctype_alpha($value);
//     filter_input(INPUT_POST, $key, FILTER_VALIDATE_FLOAT);
// endforeach;

// d($_POST);

$sunshineTypes = [
    "sunshine_sun" => "https://www.icone-png.com/png/11/10666.png",
    "sunshine_rain" => "https://www.icone-png.com/png/11/10618.png",
    "sunshine_bright" => "https://www.icone-png.com/png/11/10712.png",
    "sunshine_fog" => "https://www.icone-png.com/png/11/10776.png",
    "sunshine_snow" => "https://www.icone-png.com/png/11/10919.png",
    "sunshine_storm" => "https://www.icone-png.com/png/35/35368.png",
    "sunshine_cloudy" => "https://www.icone-png.com/png/11/10835.png"
];

$datasOk = 0;
if (preg_match('/[0-9]/', $_POST['town_name']) === 0) {
    $datasOk++;
}

if (filter_input(INPUT_POST, 'town_temperature', FILTER_VALIDATE_FLOAT) !== false) {
    $datasOk++;
}

foreach ($sunshineTypes as $sunshineType => $sunshineIcon) :
    if ($_POST['town_sunshine'] === $sunshineType) {
        $datasOk++;
    }
endforeach;

if (3 === $datasOk) {
    $f = fopen('../../datas/datas-meteo.csv', 'a');

    fputcsv($f, [$_POST['town_name'], $_POST['town_temperature'], $sunshineTypes[$_POST['town_sunshine']]], ";", '"');

    fclose($f);

    header("Location: ../index.php?status=success");
    return;
}

header("Location: ../index.php?status=error");
